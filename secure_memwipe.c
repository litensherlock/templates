#include <string.h>

void memwipe(void * ptr, int size) {
  volatile unsigned char * p = ptr;
  while (size--) {
    *p++ = 0;
  }
}

int main(int argc, char ** argv){
  char s[10];
  memcpy(s, "test_str", 9);
  memwipe(s, sizeof(s));
  return s[3];
}