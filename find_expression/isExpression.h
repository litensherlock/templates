#ifndef TEMPLATES__ISEXPRESSION_H
#define TEMPLATES__ISEXPRESSION_H

#include "../permutation.h"
#include <stdexcept>

namespace detail {

bool try_expression(std::vector<int> & numbers, const int result) noexcept;

struct merge_raii {
  merge_raii(const int idx, std::vector<int> &numbers) noexcept : m_lhs(numbers[idx]), m_idx(idx), m_numbers(numbers) {
    numbers.erase(numbers.begin() + idx);
  }

  ~merge_raii() {
    m_numbers.emplace(m_numbers.begin() + m_idx, m_lhs);
  }

private:
  const int m_lhs;
  const int m_idx;
  std::vector<int> & m_numbers;
};

template <typename... BinaryOperation>
bool try_operation(std::vector<int> & numbers, const int idx, const int result, BinaryOperation... ops) noexcept {
  const int lhs = numbers[idx];
  const int rhs = numbers[idx + 1];
  merge_raii raii(idx, numbers);

  auto checker = [&](const int lhs, const int rhs, auto callable) {
    int tmp;
    try {
      tmp = callable(lhs, rhs);
    } catch (const std::invalid_argument &) {
      return false;
    }
    numbers[idx] = tmp; // previously rhs
    const bool success = try_expression(numbers, result);
    numbers[idx] = rhs; // restore rhs again
    return success;
  };

  const bool success = (checker(lhs, rhs, ops) || ...);

  // try unary -
  if (!success) {
    const int negative_lhs = -lhs;
    return (checker(negative_lhs, rhs, ops) || ...);
  }
  return true;
}

bool try_expression(std::vector<int> & numbers, const int result) noexcept {
  if (numbers.size() == 1) {
    return numbers[0] == result;
  } else {
    using size_t = std::vector<int>::size_type;
    // merge two with some operation, recurse
    for (size_t i = 0; i < numbers.size() - 1; ++i) {
      if (try_operation(numbers, i, result,
          [](int a, int b) {return a + b; },
          [](int a, int b) {return a - b; },
          [](int a, int b) {return a * b; },
          [](int a, int b) {return b != 0 ? a / b : throw std::invalid_argument("division by zero"); }
      )) {
        return true;
      }
    }
    return false;
  }
}

} // detail

bool is_expression(std::vector<int> numbers, const int result) {
  std::sort(numbers.begin(), numbers.end());
  do {
    if (detail::try_expression(numbers, result)) return true;
  } while (getPermutation(numbers));
  return false;
}

#endif //TEMPLATES__ISEXPRESSION_H
