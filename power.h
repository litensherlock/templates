#ifndef TEMPLATES__POWER_H
#define TEMPLATES__POWER_H

// Egyptian Multiplication (Russian Peasant Algorithm)
// from sean parents talks about concurrency

template <typename Value, typename Size, typename Operation>
Value power(Value x, Size n, Operation op) {
  if (n == 0) {
    return identity_element(op);
  }

  while (n % 2 == 0) {
    n /= 2;
    x = op(x, x);
  }

  Value result = x;
  n /= 2;
  while (n != 0) {
    x = op(x, x);
    if (n % 2 != 0) {
      result = op(result, x);
    }
    n /= 2;
  }
  return result;
}

#endif //TEMPLATES__POWER_H
