
struct S {
  S() try {
    throw 2;
  } catch (...) {
    // implicit rethrow
  }

  ~S() try {
    throw 3;
  } catch (...) {
    // implicit rethrow
  }

  int fun() try {

  } catch (...) {
    // no implicit return or rethrow, must return or rethrow to not fall of the end
  }

};

int main() try {
  throw 1;
} catch (...){
  // implicit return 0
}