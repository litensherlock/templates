#ifndef TEMPLATES_RL_RANGES_H
#define TEMPLATES_RL_RANGES_H

#include <algorithm>
namespace r {

template <typename Container, typename Comparator>
void make_heap(Container && container, Comparator comp) {
  using std::begin;
  using std::end;
  std::make_heap(begin(container), end(container), comp);
}

template <typename Container, typename Comparator>
void pop_heap(Container && container, Comparator comp) {
  using std::begin;
  using std::end;
  std::pop_heap(begin(container), end(container), comp);
}

template <typename Container, typename Comparator>
void push_heap(Container && container, Comparator comp) {
  using std::begin;
  using std::end;
  std::push_heap(begin(container), end(container), comp);
}

template <typename Container, typename Value>
decltype(auto) find(Container && container, const Value & value) {
  using std::begin;
  using std::end;
  return std::find(begin(container), end(container), value);
}

template <typename Container, typename Pred>
decltype(auto) find_if(Container && container, Pred pred) {
  using std::begin;
  using std::end;
  return std::find_if(begin(container), end(container), pred);
}

template <typename Container, typename Comparator>
decltype(auto) min_element(Container && container, Comparator comp) {
  using std::begin;
  using std::end;
  return std::min_element(begin(container), end(container), comp);
}

} //r


#endif //TEMPLATES_RL_RANGES_H
