#ifndef TEMPLATES__RL_ITERATOR_TRAITS_H
#define TEMPLATES__RL_ITERATOR_TRAITS_H

#include <iterator> // iterator_traits
#include <type_traits> // is_convertible_v

namespace rl {
template <class T, class U, typename>
struct has_iterator_category_convertible_to : public std::false_type {};

template <class T, class U>
struct has_iterator_category_convertible_to<T, U, std::void_t<typename std::iterator_traits<T>::iterator_category>>
    : public std::integral_constant<bool, std::is_convertible_v<typename std::iterator_traits<T>::iterator_category, U>> {};

template <typename T>
struct is_input_iterator : public has_iterator_category_convertible_to<T, std::input_iterator_tag, void> {};

template <typename T>
inline constexpr bool is_input_iterator_v = is_input_iterator<T>::value;

template <typename T>
struct is_forward_iterator : public has_iterator_category_convertible_to<T, std::forward_iterator_tag, void> {};

template <typename T>
inline constexpr bool is_forward_iterator_v = is_forward_iterator<T>::value;
} //rl

#endif //TEMPLATES__RL_ITERATOR_TRAITS_H
