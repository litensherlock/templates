
#include <iostream>
#include <memory>
#include <vector>

struct Animal;
struct Cat;
struct Dog;
struct Llama;
struct AnimalVisitor {
  virtual void Visit(Cat*) = 0;
  virtual void Visit(Dog*) = 0;
  virtual void Visit(Llama*) = 0;
  virtual ~AnimalVisitor() = default;
};

struct Animal {
  virtual ~Animal() = default;
  virtual void Visit(AnimalVisitor*) = 0;
};

template<class T>
struct VisitableAnimal : Animal {
  void Visit(AnimalVisitor* const _visitor) override {
    _visitor->Visit(static_cast<T*>(this));
  }
};

struct Cat : VisitableAnimal<Cat>{};

struct Dog : VisitableAnimal<Dog>{};

struct Llama : VisitableAnimal<Llama>{};

struct MyCatFilter {
  void operator()(Cat* cat) {
    cats_.push_back(cat);
  }
  std::vector<Cat*> cats_;
};

template <typename T, typename = void>
struct HasCat : std::false_type {};
template <typename T>
struct HasCat<T, std::void_t<decltype(std::declval<T>()((std::declval<Cat*>())))>> : std::true_type {};

template <typename T, typename = void>
struct HasDog : std::false_type {};
template <typename T>
struct HasDog<T, std::void_t<decltype(std::declval<T>()((std::declval<Dog*>())))>> : std::true_type {};

template <typename T, typename = void>
struct HasLlama : std::false_type {};
template <typename T>
struct HasLlama<T, std::void_t<decltype(std::declval<T>()((std::declval<Llama*>())))>> : std::true_type {};

template <typename T>
struct JakobDoNothing : AnimalVisitor, T {
  using T::T;
  JakobDoNothing(T&& u) : T(std::move(u)) {}

  void Visit(Cat* const ptr) final {
    if constexpr(HasCat<T>::value) {
      (*this)(ptr);
    }
  };

  void Visit(Dog* const ptr) final {
    if constexpr(HasDog<T>::value) {
      (*this)(ptr);
    }
  };

  void Visit(Llama* const ptr) final {
    if constexpr(HasLlama<T>::value) {
      (*this)(ptr);
    }
  };
};
#define LAMBDA
int main() {
  std::vector<std::unique_ptr<Animal>> animals;
  for(size_t i = 0; i < 10; ++i)
  {
    if(i < 8)
    {
      if (i % 2 == 0)
        animals.push_back(std::make_unique<Dog>());
      else
        animals.push_back(std::make_unique<Llama>());
    }
    else
      animals.push_back(std::make_unique<Cat>());
  }
  std::vector<Cat*> cats_;
  #ifndef LAMBDA
  JakobDoNothing<MyCatFilter> visitor;
  #else
  JakobDoNothing visitor(
      [&cats_](Cat* cat) {
        cats_.emplace_back(cat);
      });
  #endif
  for (auto& animalPtr : animals)
  {
    animalPtr->Visit(&visitor);
  }
  #ifndef LAMBDA
  std::cout << "visitor encountered " << visitor.cats_.size() << " cats\n";
  #else
  std::cout << "visitor encountered " << cats_.size() << " cats\n";
  #endif
}
