#include <boost/iterator/filter_iterator.hpp>
#include <boost/iterator/transform_iterator.hpp>

#include <vector>
#include <iostream>
#include <algorithm>
#include <functional>
#include <string>

struct functor {
  bool operator()(const int a) const {
    return a < 5;
  }
};

template <typename Predicate, typename UnderlyingIterator>
class filter_iterator {
public:
  template <typename, typename>
  friend class filter_iterator;

  using iterator_category = std::forward_iterator_tag;

  /// The type "pointed to" by the iterator.
  using value_type = typename UnderlyingIterator::value_type;

  /// Distance between iterators is represented as this type.
  using difference_type = typename UnderlyingIterator::difference_type;

  /// This type represents a pointer-to-value_type.
  using pointer = typename UnderlyingIterator::pointer;

  /// This type represents a reference-to-value_type.
  using reference = typename UnderlyingIterator::reference;

  filter_iterator(UnderlyingIterator being, UnderlyingIterator end, Predicate predicate = Predicate{}) :
    m_cur(being),
    m_end(end),
    m_predicate(predicate)
    {}

  template <typename OtherIterator, typename = std::enable_if_t<std::is_convertible<OtherIterator, UnderlyingIterator>::value>>
  filter_iterator(const filter_iterator<Predicate, OtherIterator> & rhs) :
    m_cur(rhs.m_cur),
    m_end(rhs.m_end),
    m_predicate(rhs.m_predicate)
    {}

  filter_iterator& operator++() {
    increment();
    return *this;
  }

  bool operator==(filter_iterator rhs) const {
    return m_cur == rhs.m_cur;
  }

  bool operator!=(filter_iterator rhs) const {
    return !operator==(rhs);
  }

  reference operator*() const {
    return *m_cur;
  }

  difference_type operator-(const filter_iterator rhs) const {
    return m_cur - rhs.m_cur;
  }

private:
  void increment()
  {
    ++m_cur;
    satisfy_predicate();
  }

  void satisfy_predicate()
  {
    while ((m_cur != m_end) && (!m_predicate(*m_cur)))
      ++m_cur;
  }

  UnderlyingIterator m_cur;
  UnderlyingIterator m_end;
  Predicate m_predicate;
};

template <typename ValueType>
struct MySecondVec {
  using const_iterator = filter_iterator<functor, typename std::vector<ValueType>::const_iterator>;
  using iterator = filter_iterator<functor, typename std::vector<ValueType>::iterator>;

  iterator begin() {
    return iterator(vector.begin(), vector.end());
  }

  iterator end() {
    return iterator(vector.end(), vector.end());
  }

  const_iterator begin() const {
    return const_iterator(vector.begin(), vector.end());
  }

  const_iterator end() const {
    return const_iterator(vector.end(), vector.end());
  }

  int64_t index(const_iterator it) const {
    return it - begin();
  }

  std::vector<ValueType> vector;
};
struct B{};
struct A{
  int i;
  bool operator==(const A & rhs) const {
    return i == rhs.i;
  }

  bool operator==(const B &) const {
    return true;
  }

  bool operator==(const int rhs) const {
    return i == rhs;
  }
};

template <typename T, typename U, typename = std::void_t<>>
struct HasOperatorEquals : std::false_type {};

template <typename T, typename U>
struct HasOperatorEquals<T, U, std::void_t<
    decltype(std::declval<std::decay_t<T>>() == std::declval<std::decay_t<U>>())
    >> : std::true_type {};

template <typename T, typename U>
class HasOperatorEquals2 {
  static void* convertable_to_bool(bool);
  template<typename A, typename B> static std::true_type test(decltype(convertable_to_bool(std::declval<const A &>() == std::declval<const B &>())));
  template<typename A, typename B> static std::false_type test(...);

public:
  static constexpr bool value = decltype(test<T, U>(nullptr))::value;
};

template <typename A>
class MyReferenceWrapper {
public:
  MyReferenceWrapper(A &obj) noexcept : obj(obj) {}

  operator A & () noexcept {
    return obj;
  }

  template <typename T>
  std::enable_if_t<HasOperatorEquals2<A, T>::value,
  bool> operator==(const T & rhs) const {
    return obj == rhs;
  }

  template <typename T>
  std::enable_if_t<HasOperatorEquals2<A, T>::value,
  bool> operator!=(const T & rhs) const {
    return !(obj == rhs);
  }

private:
  A & obj;
};

int main(){
  std::cout << HasOperatorEquals2<A, B>::value;
  std::cout << HasOperatorEquals<A, B>::value;

  using boost::iterators::transform_iterator;
  std::vector<double> nums{ 5, 3.2, 11.5 };

  // Version 1
  std::vector<std::string> strs1(nums.size());
  std::transform(nums.begin(), nums.end(), strs1.begin(), [](double x) { return std::to_string(x); });

  auto transform = [](double x) { return std::to_string(x); };
  // Version 2
  std::vector<std::string> strs2(transform_iterator(nums.begin(), transform), transform_iterator(nums.end(), transform));

  return 0;
}