#include <type_traits> // true_type/false_type
#include <vector>
#include <algorithm>

template <typename T, typename U>
class equals_comparable {
  static void* convertable_to_bool(bool);
  template<typename A, typename B> static std::true_type test(decltype(convertable_to_bool(std::declval<const A &>() == std::declval<const B &>())));
  template<typename A, typename B> static std::false_type test(...);

public:
  static constexpr bool value = decltype(test<T, U>(nullptr))::value;
};


template <typename T>
struct ReverseEquals {
  ReverseEquals(T &val) : val(val) {}

  template <typename U>
  std::enable_if_t<equals_comparable<T, U>::value,
  bool> operator()(const U & rhs) const {
    return val == rhs;
  }
  const T & val;
};

struct C {};
namespace a {

struct B {};


namespace b {
struct A {
  bool operator==(const B &) const {
    return true;
  }

  bool operator==(const int) const {
    return true;
  }
};

}

}

bool operator==(const a::B&, const a::b::A &) {
  return true;
}

template <typename A, typename B>
bool eq(const A & a, const B & b) {
  return a == b;
}

int main() {
  std::vector<a::B> bs(5);
  std::vector<int> ints(5);
  std::vector<C> cs(5);


  a::b::A a;

  return std::find(bs.begin(), bs.end(), a) == bs.end();
}