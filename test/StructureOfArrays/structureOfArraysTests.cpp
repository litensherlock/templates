#include <gtest/gtest.h>

#include "../../StructureOfArrays.h"

TEST(SoA, erase) {
  //setup
  SoA soa;
  soa.add(1, 1, 5.0);
  soa.add(7, 0, 2.0);
  soa.add(12, 1, 30.0);

  //exec
  soa.erase(soa.begin() + 1);

  //verify
  EXPECT_EQ(soa.size(), 2);

  auto it = soa.begin();
  EXPECT_EQ(it->id(), 1);
  EXPECT_EQ(it->value(), 5.0);
  EXPECT_EQ(it->isCool(), 1);

  ++it;
  EXPECT_EQ(it->id(), 12);
  EXPECT_EQ(it->value(), 30.0);
  EXPECT_EQ(it->isCool(), 1);
}

TEST(SoA, stepOperator) {
  //setup
  SoA soa;
  soa.add(1, 1, 5.0);
  auto it = soa.begin();

  //exec
  ++it;

  //verify
  EXPECT_EQ(it, soa.end());
}

TEST(SoA, convertNonConstToConst) {
  //setup
  SoA soa;
  soa.add(1, 1, 5.0);
  auto it = soa.begin();

  //exec
  SoA::const_iterator cit = it;

  //verify
  EXPECT_EQ(cit, ((const SoA *)&soa)->begin());
}

TEST(SoA, iteratorStepForward) {
  //setup
  SoA soa;
  soa.add(1, 1, 5.0);
  auto it = soa.begin();

  //exec
  ++it;

  //verify
  EXPECT_EQ(it, soa.end());
}

TEST(SoA, iteratorForwardsJump) {
  //setup
  SoA soa;
  soa.add(1, 1, 5.0);
  soa.add(2, 1, 5.0);
  soa.add(3, 1, 5.0);
  auto it = soa.begin();

  //exec
  auto test_end = it + 3;

  //verify
  EXPECT_EQ(test_end, soa.end());
  EXPECT_EQ(it, soa.begin());
}

TEST(SoA, iteratorStepBackward) {
  //setup
  SoA soa;
  soa.add(1, 1, 5.0);
  auto it = soa.end();

  //exec
  --it;

  //verify
  EXPECT_EQ(it, soa.begin());
}

TEST(SoA, iteratorBackwardsJump) {
  //setup
  SoA soa;
  soa.add(1, 1, 5.0);
  soa.add(2, 1, 5.0);
  soa.add(3, 1, 5.0);
  auto it = soa.end();

  //exec
  auto test_begin = it - 3;

  //verify
  EXPECT_EQ(test_begin, soa.begin());
  EXPECT_EQ(it, soa.end());
}


int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}