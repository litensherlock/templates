#include <gtest/gtest.h>

#include "../trapped_water/trapped_water.h"


TEST(trapped_water, zero) {
  const int expected = 0;
  std::array arr{1, 2, 3, 4};
  const int water = calculate_trapped_water(arr.begin(), arr.end());
  EXPECT_EQ(expected, water);
}

TEST(trapped_water, zero_right) {
  const int expected = 0;
  std::array arr{4, 3, 3, 2};
  const int water = calculate_trapped_water(arr.begin(), arr.end());
  EXPECT_EQ(expected, water);
}

TEST(trapped_water, one) {
  const int expected = 1;
  std::array arr{3, 2, 3, 4};
  const int water = calculate_trapped_water(arr.begin(), arr.end());
  EXPECT_EQ(expected, water);
}

TEST(trapped_water, rugged_simple) {
  const int expected = 8;
  std::array arr{2, 6, 3, 5, 2, 8};
  const int water = calculate_trapped_water(arr.begin(), arr.end());
  EXPECT_EQ(expected, water);
}

TEST(trapped_water, rugged) {
  const int expected = 27;
  std::array arr{8, 1, 4, 2, 2, 5, 3, 5, 7, 4, 1};
  const int water = calculate_trapped_water(arr.begin(), arr.end());
  EXPECT_EQ(expected, water);
}

TEST(trapped_water, rugged_2) {
  const int expected = 35;
  std::array arr{2, 6, 3, 5, 2, 8, 1, 4, 2, 2, 5, 3, 5, 7, 4, 1};
  const int water = calculate_trapped_water(arr.begin(), arr.end());
  EXPECT_EQ(expected, water);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}