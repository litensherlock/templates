#include <benchmark/benchmark.h>

#include "../fibonacci/simple_fibonacci.h"
#include "../fibonacci/fibonacci.h"

static void BM_power_fibonacci(benchmark::State & state) {
  for (auto _ : state) {
    // This code gets timed
    benchmark::DoNotOptimize(fibonacci<unsigned long>(state.range()));
  }
}

static void BM_simple_fibonacci(benchmark::State & state) {
  for (auto _ : state) {
    // This code gets timed
    benchmark::DoNotOptimize(simple_fibonacci<unsigned long>(state.range()));
  }
}

// Register the function as a benchmark
BENCHMARK(BM_power_fibonacci)->Range(10, 200);
BENCHMARK(BM_simple_fibonacci)->Range(10, 200);

// Run the benchmark
BENCHMARK_MAIN();