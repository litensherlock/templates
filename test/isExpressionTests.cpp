#include <gtest/gtest.h>

#include "../find_expression/isExpression.h"


TEST(find_expression, simple) {
  const bool expected = true;
  std::vector<int> arr{1, 2};
  const int total = 3;
  const bool res = is_expression(arr, total);
  EXPECT_EQ(expected, res);
}

TEST(find_expression, simple2) {
  const bool expected = true;
  std::vector<int> arr{1, 3, 2};
  const int total = 8;
  const bool res = is_expression(arr, total);
  EXPECT_EQ(expected, res);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}