#include <gtest/gtest.h>

#include "../fibonacci/fibonacci.h"
#include "../fibonacci/simple_fibonacci.h"

TEST(fib, small_test) {
  const int result = fibonacci<int>(3);
  EXPECT_EQ(result, 2);
}

TEST(fib, small_comp3) {
  const int n = 3;
  EXPECT_EQ(fibonacci<int>(n), simple_fibonacci<int>(n));
}

TEST(fib, small_comp4) {
  const int n = 4;
  EXPECT_EQ(fibonacci<int>(n), simple_fibonacci<int>(n));
}

TEST(fib, medium_comp18) {
  const int n = 18;
  EXPECT_EQ(fibonacci<int>(n), simple_fibonacci<int>(n));
}

std::string arrange(std::string sentence) {
  unsigned long prev = 0;
  unsigned long i = 0;
  std::vector<std::string> tmp;
  i = sentence.find(' ', prev);
  while (i != std::string::npos) {
    tmp.emplace_back(sentence.substr(prev, i - prev));
    prev = i + 1;
    i = sentence.find(' ', prev);
  }

  if (prev < sentence.size()) {
    tmp.emplace_back(sentence.substr(prev));
  }

  //remove the dot from the last word and make the first word have a small letter
  if (!tmp.empty()) {
    tmp[0][0] = tolower(tmp[0][0]);
    std::string & lastWord = tmp[tmp.size() - 1];
    lastWord.erase(lastWord.end() - 1);
  }

  std::stable_sort(tmp.begin(), tmp.end(), [](const std::string & a, const std::string & b) { return a.size() < b.size();});

  if (!tmp.empty()) {
    tmp[0][0] = toupper(tmp[0][0]);
    std::string & lastWord = tmp[tmp.size() - 1];
    lastWord += ".";
  }

  std::string result;
  result.reserve(sentence.size());
  for (auto it = tmp.begin(); it != tmp.end(); it++) {
    result += *it;
    if (it + 1 != tmp.end()) {
      result += " ";
    }
  }
  return result;
}

int getMinimumUniqueSum(std::vector<int> arr) {
  if (arr.empty()) {
    return 0;
  } if (arr.size() < 2) {
    return arr[0];
  }

  std::sort(arr.begin(), arr.end());
  auto it = arr.begin();
  auto trailer = it++;
  int counter = 0;
  int startOfRow = -1;
  for(; it != arr.end(); ++it, ++trailer) {
    if ((startOfRow != -1) && (startOfRow == *it)) {
      // while the same number is repeated, simply increase the counter
      counter++;
      *it += counter;
    } else if ((startOfRow != -1) && (startOfRow >= *it ||  *it <= startOfRow + counter)) {
      // if we have been on a repeating sequence,
      // and the next number is somewhere in the now unified sequence, then continue with the sequence
      counter++;
      *it = counter;
    } else if (*trailer == *it) {
      // start of a new sequence, two equal numbers are found
      // increase the counter and move along
      startOfRow = *trailer;
      counter = 1;
      *it += counter;
    } else {
      // not in a sequence
      startOfRow = -1;
      counter = 0;
    }
  }

  int sum = 0;
  for (const auto &item : arr) {
    sum += item;
  }
  return sum;
}

int countPairs(std::vector<int> numbers, int k) {
  std::set<int> index;
  std::set<std::pair<int, int>> uniquePairs;
  for (int number : numbers) {
    index.insert(number);
  }

  for (const int & number : numbers) {
    auto it = index.find(number + k);
    if (it != index.end()) {
      uniquePairs.insert(std::make_pair(number, *it));
    }
  }

  return uniquePairs.size();
}

TEST(hackerank, test) {
  std::string input = "The lines are printed in reverse order.";
  std::string output = arrange(input);
  EXPECT_EQ("In the are lines order printed reverse.", output);
}

TEST(hackerank, tes2t) {
  std::string input = "Here i come.";
  std::string output = arrange(input);
  EXPECT_EQ("I here come.", output);
}


TEST(hackerank, test3) {
  std::vector<int> input{1, 2, 2};
  int output = getMinimumUniqueSum(input);
  EXPECT_EQ(6, output);
}


TEST(hackerank, test4) {
  std::vector<int> input{2, 2, 4, 5};
  int output = getMinimumUniqueSum(input);
  EXPECT_EQ(14, output);
}


TEST(hackerank, test5) {
  std::vector<int> input{1, 1, 2, 2, 3, 3};
  int output = countPairs(input, 1);
  EXPECT_EQ(2, output);
}

TEST(hackerank, test6) {
  std::vector<int> input{1, 2, 3, 4, 5, 6};
  int output = countPairs(input, 2);
  EXPECT_EQ(4, output);
}

TEST(hackerank, test7) {
  std::vector<int> input{1, 2, 5, 6, 9, 10};
  int output = countPairs(input, 2);
  EXPECT_EQ(0, output);
}


TEST(fib, medium_comp48) {
  const int n = 48;
  using return_t = long;
  EXPECT_EQ(fibonacci<return_t>(n), simple_fibonacci<return_t>(n));
}

TEST(fib, comp200) {
  const int n = 200;
  using return_t = unsigned long;
  EXPECT_EQ(fibonacci<return_t>(n), simple_fibonacci<return_t>(n));
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}