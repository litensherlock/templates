#include <gtest/gtest.h>

#include "../permutation.h"


TEST(permutations, simple) {
  const std::array expected{1, 2, 4, 3};
  std::array value{1, 2, 3, 4};
  const bool sts = getPermutation(value.begin(), value.end());
  EXPECT_EQ(expected, value);
  EXPECT_EQ(true, sts);
}

TEST(permutations, simple_end) {
  const std::array expected{4, 3, 2, 1};
  std::array value{4, 3, 2, 1};
  const bool sts = getPermutation(value.begin(), value.end());
  EXPECT_EQ(expected, value);
  EXPECT_EQ(false, sts);
}


TEST(permutations, simple_range) {
  const std::array expected{1, 2, 4, 3};
  std::array value{1, 2, 3, 4};
  const bool sts = getPermutation(value);
  EXPECT_EQ(expected, value);
  EXPECT_EQ(true, sts);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}