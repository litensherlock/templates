#ifndef TEMPLATES__MYVECTOR_H
#define TEMPLATES__MYVECTOR_H

#include <memory> // for allocator traits
#include <utility> // for swap
#include "rl/rl_iterator_traits.h" // is_input_iterator

template <typename T, typename Allocator = std::allocator<T>>
class MyVector {
  static constexpr auto growth_factor = 1.5;
  using alloc_traits = std::allocator_traits<Allocator>;
public:
  using value_type = T;
  using allocator_type = Allocator;
  using size_type = size_t;
  using difference_type = ptrdiff_t;

  using pointer = typename alloc_traits::pointer;
  using const_pointer = typename alloc_traits::const_pointer;
  using reference = T&;
  using const_reference = const T&;
  using iterator = pointer;
  using const_iterator = const_pointer;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  MyVector() noexcept(noexcept(Allocator())) :
      m_begin{},
      m_end{},
      m_end_of_storage{},
      m_alloc{} {}

  explicit MyVector(const Allocator& allocator) noexcept :
      m_begin{},
      m_end{},
      m_end_of_storage{},
      m_alloc(allocator) {}

  MyVector(const size_type count, const T& value, const Allocator& alloc = Allocator()) :
      m_begin{},
      m_end{},
      m_end_of_storage{},
      m_alloc(alloc)
  {
    assign(count, value);
  }

  explicit MyVector(const size_type count, const Allocator& alloc = Allocator()) :
      m_begin{},
      m_end{},
      m_end_of_storage{},
      m_alloc(alloc)
  {
    reserve(count);
  }

  template <typename InputIt, typename = std::enable_if_t<rl::is_input_iterator_v<InputIt>>>
  MyVector(InputIt begin, InputIt end, const Allocator& alloc = Allocator()) :
      m_begin{},
      m_end{},
      m_end_of_storage{},
      m_alloc(alloc)
  {
    assign(begin, end);
  }

  MyVector(const MyVector& other) :
      m_begin{},
      m_end{},
      m_end_of_storage{},
      m_alloc(alloc_traits::select_on_container_copy_construction(other.get_allocator()))
  {
    *this = other;
  }

  MyVector(const MyVector& other, const Allocator& alloc) :
      m_begin{},
      m_end{},
      m_end_of_storage{},
      m_alloc(alloc)
  {
    *this = other;
  }

  MyVector(MyVector&& other) noexcept :
      m_begin(other.m_begin),
      m_end(other.m_end),
      m_end_of_storage(other.m_end_of_storage),
      m_alloc(std::move(other.m_alloc))
  {
    other.m_begin = nullptr;
    other.m_end = nullptr;
    other.m_end_of_storage = nullptr;
  }

  MyVector(MyVector&& other, const Allocator& alloc) :
      m_begin{},
      m_end{},
      m_end_of_storage{},
      m_alloc(alloc)
  {
    // if the allocator is equal, then steal the memory. Otherwise do a element wise move
    if(m_alloc == other.get_allocator()) {
      using namespace std;
      swap(m_begin, other.m_begin);
      swap(m_end, other.m_end);
      swap(m_end_of_storage, other.m_end_of_storage);
    } else {
      for(auto && elem : other) {
        emplace_back(std::move(elem));
      }
      other.clear();
    }
  }

  MyVector(std::initializer_list<T> ilist, const Allocator& alloc = Allocator()) :
      m_begin{},
      m_end{},
      m_end_of_storage{},
      m_alloc(alloc)
  {
    assign(ilist.begin(), ilist.end());
  }

  ~MyVector() {
    clearAndDeallocate();
  }

  MyVector& operator=(const MyVector& other) {
    this->clear();
    this->reserve(other.size());
    for (const value_type &elem : other) {
      this->push_back(elem);
    }
  }

  // Note: that the two boolean expressions below are equal.
  // They are written like this to be equal to the wording in the documentation
  MyVector& operator=(MyVector&& other) noexcept(
      alloc_traits::propagate_on_container_move_assignment::value ||
      alloc_traits::is_always_equal::value) {

    clearAndDeallocate();
    if constexpr (!alloc_traits::propagate_on_container_move_assignment::value &&
                  !alloc_traits::is_always_equal::value) {
      // need to move over the elements to our allocator
      for (const value_type &item : other) {
        push_back(item);
      }
    } else {
      using namespace std;
      swap(m_begin, other.m_begin);
      swap(m_end, other.m_end);
      swap(m_end_of_storage, other.m_end_of_storage);

      if constexpr (alloc_traits::propagate_on_container_move_assignment::value) {
        m_alloc = std::move(other.m_alloc);
      }
    }
  }

  MyVector& operator=(std::initializer_list<T> ilist) {
    clearAndDeallocate();
    for (const auto &item : ilist) {
      push_back(item);
    }
  }

  void assign(const size_type count, const T& value) {
    if (count > max_size()) {
      throw std::length_error("count must be smaller than max_size()");
    }

    clear();
    reserve(count);

    for (size_type i = 0; i < count; ++i)
      push_back(value);
  }

  template <typename InputIt>
  std::enable_if_t<rl::is_input_iterator_v<InputIt>,
  void> assign(InputIt begin, InputIt end) {
    clear();

    while (begin != end) {
      push_back(*begin);
      ++begin;
    }
  }

  void assign(std::initializer_list<T> ilist) {
    assign(ilist.begin(), ilist.end());
  }

  allocator_type get_allocator() const noexcept(noexcept(allocator_type(std::declval<allocator_type>()))) {
     return m_alloc;
  }

  /*****************************************
   *             element access            *
   *****************************************/

  reference at(const size_type n) {
    range_check(n);
    return *(this)[n];
  }

  const_reference at(const size_type n) const {
    range_check(n);
    return *(this)[n];
  }

  reference operator[](const size_type n) noexcept {
    return *(begin() + n);
  }

  const_reference operator[](const size_type n) const noexcept {
    return *(begin() + n);
  }

  reference front() noexcept {
    return *begin();
  }

  const_reference front() const noexcept {
    return *begin();
  }

  reference back() noexcept {
    return *(end() - 1);
  }

  const_reference back() const noexcept {
    return *(end() - 1);
  }

  pointer data() noexcept {
    return begin();
  }

  const_pointer data() const noexcept {
    return begin();
  }

  /*****************************************
   *             iterators                 *
   *****************************************/

  iterator begin() noexcept {
    return m_begin;
  }

  iterator end() noexcept {
    return m_end;
  }

  const_iterator begin() const noexcept {
    return m_begin;
  }

  const_iterator end() const noexcept {
    return m_end;
  }

  const_iterator cbegin() const noexcept {
    return m_begin;
  }

  const_iterator cend() const noexcept {
    return m_end;
  }

  reverse_iterator rbegin() noexcept {
    return reverse_iterator(m_end);
  }

  reverse_iterator rend() noexcept {
    return reverse_iterator(m_begin);
  }

  const_reverse_iterator rbegin() const noexcept {
    return crbegin();
  }

  const_reverse_iterator rend() const noexcept {
    return crend();
  }

  const_reverse_iterator crbegin() const noexcept {
    return reverse_iterator(m_end);
  }

  const_reverse_iterator crend() const noexcept {
    return reverse_iterator(m_begin);
  }

  /*****************************************
   *             capacity                  *
   *****************************************/

  [[nodiscard]] bool empty() const noexcept {
    return begin() == end();
  }

  size_type size() const noexcept {
    return static_cast<size_type>(m_end - m_begin);
  }

  size_type max_size() const noexcept {
    return alloc_traits::max_size(m_alloc);
  }

  void reserve(const size_type n) {
    if (n > max_size()) {
      throw std::length_error("n must be smaller than max_size()");
    }
    if (n > this->capacity())
      this->reallocate(n);
  }

  size_type capacity() const noexcept {
    return static_cast<size_type>(m_end_of_storage - m_begin);
  }

  void shrink_to_fit() {
    if(size() < capacity())
      reallocate(size());
  }

  /*****************************************
   *           modifiers                   *
   *****************************************/

  void clear() noexcept(noexcept(alloc_traits::destroy(std::declval<allocator_type&>(), std::declval<pointer>()))) {
    if (empty())
      return;

    // back the end pointer down to the beginning
    do {
      --m_end;
      alloc_traits::destroy(m_alloc, m_end);
    } while (m_begin != m_end);
  }

  iterator insert(const_iterator pos, const T& value) {
    return insert(pos, 1, value);
  }

  iterator insert(const_iterator pos, T&& value) {
    return emplace(pos, std::move(value));
  }

  iterator insert(const_iterator pos, size_type count, const T& value) {
    const size_type index = pos - begin();
    reserve(size() + count);
    iterator nc_pos = begin() + index;
    if (nc_pos == end()) {
      for (; count != 0; --count)
        push_back(value);
      return end() - 1;
    }

    const_iterator old_end = move_range_forwards(nc_pos, end(), nc_pos + count);
    nc_pos = begin() + index;
    // assign in the range [pos, old end()[
    for(; (nc_pos != old_end) && (count != 0); ++nc_pos, --count)
      *nc_pos = value;

    // construct in the range [old end(), end[
    for(; count != 0; ++nc_pos, --count)
      alloc_traits::construct(m_alloc, nc_pos, value);

    return nc_pos;
  }

  template <typename InputIt>
  std::enable_if_t<rl::is_input_iterator_v<InputIt>,
  iterator> insert(const_iterator pos, InputIt begin, InputIt end) {
    size_type index = pos - this->begin();

    // get the number of elements if possible to avoid multiple moves and reallocations,
    // otherwise fallback to inserting one element at the time
    if constexpr (rl::is_forward_iterator_v<InputIt>) {
      const difference_type input_size = std::distance(begin, end);
      reserve(size() + input_size);
      pos = begin() + index;
      const_iterator end_move_iterator = std::min(pos + input_size, end());
      const_iterator old_end = move_range_forwards(pos, end(), pos + input_size);

      // assign in the range [pos, old end()[
      for(; (pos != old_end) && (begin != end); ++pos, ++begin)
        *pos = *begin;

      // construct in the range [old end(), end[
      for(; begin != end; ++pos, ++begin)
        alloc_traits::construct(m_alloc, pos, *begin);
    } else {
      for(; begin != end; ++begin, ++index)
        insert(this->begin() + index, *begin);
    }

    return back();
  }

  iterator insert(const_iterator pos, std::initializer_list<T> ilist) {
    return insert(pos, ilist.begin(), ilist.end());
  }

  template <typename... Args>
  iterator emplace(const_iterator pos, Args&&... args) {
    // note, possible future optimization
    // if this would lead to a reallocation,
    // then the new element could be inserted while constructing the elements on the new location.

    const size_type index = pos - begin(); // store the index to have a valid point of reference even after possible iterator invalidation
    reserve(size() + 1);
    iterator nc_pos = begin() + index;
    if (nc_pos != end())
      move_range_forwards(nc_pos, end(), nc_pos + 1);

    alloc_traits::construct(m_alloc, begin() + index, std::forward<Args>(args)...);
    return nc_pos;
  }

  iterator erase(const_iterator position) {
    return erase(position, position + 1);
  }

  iterator erase(const_iterator begin, const_iterator end) {
    // move elements following the end iterator down to the begin iterator
    const_iterator trailer = begin;
    for(const_iterator cur = end; cur != end(); ++trailer, ++cur) {
      *trailer = std::move(*cur);
    }

    // trailer now marks the new end, store it for later use
    iterator new_end = trailer;

    // destruct objects that are now outside the range
    while (trailer != end()) {
      alloc_traits::destroy(m_alloc, trailer);
      ++trailer;
    }

    // move the end marker
    m_end = new_end;
  }

  void push_back(const value_type& x) {
    if(capacity() == size()) {
      grow();
    }
    alloc_traits::construct(m_alloc, m_end, x);
    ++m_end;
  }

  void push_back(value_type&& x) {
    emplace_back(std::move(x));
  }

  template <typename... Args>
  reference emplace_back(Args&&... args) {
    if(capacity() == size()) {
      grow();
    }
    alloc_traits::construct(m_alloc, m_end, std::forward<Args>(args)...);
    return *(m_end++);
  }

  void pop_back() noexcept(noexcept(alloc_traits::destroy(std::declval<allocator_type&>(), std::declval<pointer>()))) {
    --m_end;
    alloc_traits::destroy(m_alloc, m_end);
  }

  /* todo add strong exception guarantee on both resize overloads https://en.cppreference.com/w/cpp/container/vector/resize
   * In overload (1), if T's move constructor is not noexcept and T is not CopyInsertable into *this, vector will use the throwing move constructor.
   * If it throws, the guarantee is waived and the effects are unspecified.
   */
  void resize(const size_type count) {
    if (this->size() < count) {
      reserve(count);
      const difference_type missing = count - this->size();
      for (difference_type i = 0; i < missing; ++i) {
        emplace_back();
      }
    } else { // count is less or equal to size()
      const difference_type overflow = this->size() - count;
      for (difference_type i = 0; i < overflow; ++i) {
        pop_back();
      }
    }
  }

  void resize(const size_type count, const value_type & value) {
    if (this->size() < count) {
      reserve(count);
      const difference_type missing = count - this->size();
      for (difference_type i = 0; i < missing; ++i) {
        emplace_back(value);
      }
    } else { // count is less or equal to size()
      const difference_type overflow = this->size() - count;
      for (difference_type i = 0; i < overflow; ++i) {
        pop_back();
      }
    }
  }

  void swap(MyVector& other) noexcept(alloc_traits::propagate_on_container_swap::value
                                      || alloc_traits::is_always_equal::value) {
    using namespace std;
    swap(this->m_begin, other.m_begin);
    swap(this->m_end, other.m_end);
    swap(this->m_end_of_storage, other.m_end_of_storage);
    if constexpr (alloc_traits::propagate_on_container_swap::value) {
      swap(this->m_alloc, other.m_alloc);
    }
  }

private:
  void grow() {
    // increase capacity by at least one
    const size_type n =
        std::max(
            static_cast<size_type>(std::floor(capacity() * growth_factor)),
            capacity() + 1);
    reallocate(n);
  }

  void reallocate(const size_type n) {
    const pointer new_begin = alloc_traits::allocate(m_alloc, n);
    const pointer new_end_of_storage = new_begin + n;

    // copy all objects from the old storage to the new storage
    pointer new_cur = new_begin;
    for(pointer cur = m_begin; cur != m_end; ++cur, ++new_cur) {
      alloc_traits::construct(m_alloc, new_cur, *cur);
    }
    // grab the new end iterator
    pointer new_end = new_cur;

    this->clearAndDeallocate();

    // move over to use the new storage
    m_begin = new_begin;
    m_end = new_end;
    m_end_of_storage = new_end_of_storage;
  }

  // use the end of storage pointer to check if the vector has any storage.
  // if not, then there is nothing to clear or deallocate
  void clearAndDeallocate() {
    if (m_end_of_storage == nullptr) {
      return;
    }
    this->clear();
    alloc_traits::deallocate(m_alloc, begin(), capacity());
    m_begin = nullptr;
    m_end = nullptr;
    m_end_of_storage = nullptr;
  }

  void range_check(const size_type n) const {
    if (n >= size()) {
      throw std::out_of_range("n is bigger than current size of vector");
    }
  }

  // assumes that from_begin is within [begin(), end()[, and from_end is within [begin(), end()]
  // does not destruct any objects
  // returns iterator pointing one last the end of the elements before the move
  iterator move_range_forwards(iterator from_begin, iterator from_end, iterator to_begin) {
    if(from_begin == from_end) {
      return m_end;
    }

    iterator to_end = to_begin + (from_end - from_begin);
    iterator to_end_backup = to_end; // used to set the end member variable later

    // move construct objects outside the current range
    while ((to_end != end()) && (from_begin != from_end)) {
      --to_end;
      --from_end;
      alloc_traits::construct(m_alloc, to_end, std::move(*from_end));
    }

    // move assign to objects inside the already constructed current range
    while (from_begin != from_end) {
      --to_end;
      --from_end;
      *to_end = std::move(*from_end);
    }

    // set end to keep the vector in a consistent state, and return the old end iterator
    iterator old_end = m_end;
    m_end = to_end_backup;
    return old_end;
  }

  pointer m_begin;
  pointer m_end;
  pointer m_end_of_storage;
  Allocator m_alloc;
};

#endif //TEMPLATES__MYVECTOR_H
