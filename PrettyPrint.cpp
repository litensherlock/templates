
#include <type_traits> // for std::void_t
#include <ostream> // for ostream
#include <iostream>


template <typename T, typename = std::void_t<>>
struct HasPrettyPrint : std::false_type {};

template <typename T>
struct HasPrettyPrint<T, std::void_t<decltype(operator<<(std::declval<std::ostream>(), std::declval<const T>()))>> : std::true_type {};

struct TStruct {};

std::ostream & operator<<(std::ostream & stream, const TStruct &) {
  return stream;
}

struct S {
  ~S() {
    std::cout << "~S()\n";
  }
};

template <typename Functor>
auto fun(Functor && f){
  return f();
}

auto creatorFunction(const S &){
  return []() {
    std::cout << "lambda\n";
  };
}

int main() {
  fun(creatorFunction(S{}));
  return 0;
}