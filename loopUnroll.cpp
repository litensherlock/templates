#include <utility> // for index_sequence
#include <cstdio>  // for size_t
#include <cassert> // for assert

void fun(int i) {
  printf("process(%d)\n", i);
}

namespace detail {
template<typename Function, size_t... Is>
void unroll_batch(Function & function, const int i, std::index_sequence<Is...>) {
  (function(i + Is), ...);
}

template<typename Function>
void unroll_remainder(Function& f, const int remainder, const int count) {
  // This is the trick used by MPark.Variant:
  // https://mpark.github.io/programming/2019/01/22/variant-visitation-v2/
  assert(remainder < 8);
  switch (remainder) {
    case 7 : unroll_batch(f, count, std::make_index_sequence<7>{}); break;
    case 6 : unroll_batch(f, count, std::make_index_sequence<6>{}); break;
    case 5 : unroll_batch(f, count, std::make_index_sequence<5>{}); break;
    case 4 : unroll_batch(f, count, std::make_index_sequence<4>{}); break;
    case 3 : unroll_batch(f, count, std::make_index_sequence<3>{}); break;
    case 2 : unroll_batch(f, count, std::make_index_sequence<2>{}); break;
    case 1 : unroll_batch(f, count, std::make_index_sequence<1>{}); break;
    default: break;
  }
}

} // detail

template <int BatchSize, typename Callable>
void unroll(const int count, Callable & callable) {
  const int reminder = count % BatchSize;
  const int clamped = count - reminder;
  for(int i = 0; i < clamped; i += BatchSize){
    detail::unroll_batch(callable, i, std::make_index_sequence<BatchSize>{});
  }
  detail::unroll_remainder(callable, reminder, clamped);
}


int main() {
  constexpr int const iterations = 50;
  unroll<8>(iterations, fun);
}
