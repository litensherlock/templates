#define UNW_LOCAL_ONLY
#include <libunwind.h>
#include <cstdio>

void show_backtrace () {
  unw_cursor_t cursor; unw_context_t uc;
  unw_word_t offset;
  unw_word_t first_argument;
  bool first = true;

  unw_getcontext(&uc);
  unw_init_local(&cursor, &uc);
  while (unw_step(&cursor) > 0) {
    if (first) {
      first = false;
      const int getRegSts = unw_get_reg(&cursor, UNW_X86_64_RDI, &first_argument);
      if (getRegSts == 0) {
        printf("%s=%lx\n", unw_regname(UNW_X86_64_RDI), first_argument);
      } else {
        switch (getRegSts) {
          case UNW_EBADREG:
            printf("failed to get %s (UNW_EBADREG)\n", unw_regname(UNW_X86_64_RDI));
            break;
          case UNW_EUNSPEC:
            printf("failed to get %s (UNW_EUNSPEC)\n", unw_regname(UNW_X86_64_RDI));
            break;
          default:
            printf("failed to get %s (unknown error)\n", unw_regname(UNW_X86_64_RDI));
            break;
        }
      }
      
    }
    char sym[256];
    if (unw_get_proc_name(&cursor, sym, sizeof(sym), &offset) == 0) {
      printf(" (%s+0x%lx)\n", sym, offset);
    } else {
      printf(" -- error: unable to obtain symbol name for this frame\n");
    }
  }
  printf("%s", "\n");
}

struct TestClass {

  explicit TestClass(int refCount) : ref_count(refCount) {
    show_backtrace();
  }

  void addRef() {
    ++ref_count;
    show_backtrace();
  }

  void removeRef() {
    --ref_count;
    show_backtrace();
    if (ref_count == 0) {
      delete this;
    }
  }

private:
  int ref_count;
};

struct ref_raii {
  explicit ref_raii(TestClass *const ptr) : ptr(ptr) {
    ptr->addRef();
  }

  ~ref_raii() {
    ptr->removeRef();
  }

  TestClass * ptr;
};

void q(TestClass * testClass) {
  ref_raii s(testClass);
}

void f(TestClass * testClass) {
  ref_raii s(testClass);
  q(s.ptr);
}

TestClass * factory() {
  return new TestClass(1);
}


int main() {
  ref_raii raii(factory());
  f(raii.ptr);
  q(raii.ptr);
  raii.ptr->removeRef();
  return 0;
}
