#ifndef ADVENT_OF_CODE_2019__PERMUTATION_H
#define ADVENT_OF_CODE_2019__PERMUTATION_H

#include <algorithm> // for std::reverse, and std::swap
#include <iterator> // for std::begin, and std::end

/* implemented from description
 * https://en.wikipedia.org/wiki/Permutation#Generation_in_lexicographic_order
 */
namespace detail {
template<typename Iterator>
Iterator findAklessThenAkPlusOne(Iterator begin, const Iterator end) {
  Iterator idx = end;
  Iterator trailer = begin;
  for (++begin; begin != end; ++begin, ++trailer) {
    if (*trailer < *begin) {
      idx = trailer;
    }
  }
  return idx;
}

template<typename BiDirectionalIt>
BiDirectionalIt findI(const BiDirectionalIt begin, BiDirectionalIt end) {
  BiDirectionalIt idx = end;
  for (--end; begin != end; --end) {
    if (*begin < *end) {
      return end;
    }
  }
  return idx;
}

} // detail

/**
 * @return false if no more permutations are available, true otherwise
 */
template <typename BiDirectionalIterator>
bool getPermutation(BiDirectionalIterator begin, const BiDirectionalIterator end) {
  auto k = detail::findAklessThenAkPlusOne(begin, end);

  if (k == end){
    return false;
  } else {
    auto i = detail::findI(k, end);
    using std::swap;
    swap(*i, *k);
    std::reverse(k + 1, end);
    return true;
  }
}

/**
 * @return false if no more permutations are available, true otherwise
 */
template <typename Container>
bool getPermutation(Container & c) {
  using std::begin;
  using std::end;
  return getPermutation(begin(c), end(c));
}

#endif //ADVENT_OF_CODE_2019__PERMUTATION_H
