#ifndef TEMPLATES__SIMPLE_FIBONACCI_H
#define TEMPLATES__SIMPLE_FIBONACCI_H

template <typename R, typename N>
R simple_fibonacci(const N n) {
  if(n <= 0) {
    return R(0);
  } else if(n < 2) {
    return R(1);
  } else {
    R cur;
    R prev1 = 0;
    R prev2 = 1;
    for(N counter = 1; counter != n; ++counter) {
       cur = prev1 + prev2;
       prev1 = prev2;
       prev2 = cur;
    }
    return cur;
  }
}

#endif //TEMPLATES__SIMPLE_FIBONACCI_H
