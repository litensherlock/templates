#ifndef TEMPLATES__FIBONACCI_H
#define TEMPLATES__FIBONACCI_H

#include "../power.h"
#include <array>

template <typename N>
struct multiply_2x2 {
  std::array<N, 4> operator()(const std::array<N, 4>& x, const std::array<N, 4>& y) {
    return { x[0] * y[0] + x[1] * y[2], x[0] * y[1] + x[1] * y[3],
             x[2] * y[0] + x[3] * y[2], x[2] * y[1] + x[3] * y[3] };
  }
};

template <typename T>
std::array<T, 4> identity_element(const multiply_2x2<T>&) { return {T(1), T(0), T(0), T(1) }; }

template <typename R, typename Size>
R fibonacci(Size n) {
  if (n == 0) return R(0);
  return power( std::array<R, 4>{ 1, 1, 1, 0 }, Size(n - 1), multiply_2x2<R>())[0];
}


#endif //TEMPLATES__FIBONACCI_H
