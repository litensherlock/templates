#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#define UNW_LOCAL_ONLY
#include <libunwind.h>

void show_backtrace () {
  unw_cursor_t cursor; unw_context_t uc;
  unw_word_t offset;

  unw_getcontext(&uc);
  unw_init_local(&cursor, &uc);
  while (unw_step(&cursor) > 0) {
    char sym[256];
    if (unw_get_proc_name(&cursor, sym, sizeof(sym), &offset) == 0) {
      printf(" %s+0x%lx\n", sym, offset);
    } else {
      printf(" -- error: unable to obtain symbol name for this frame\n");
    }
  }
  printf("%s", "\n");
}

void sig_handler(int signum, siginfo_t *, void *) {
  printf("Received signal %d\n", signum);
  show_backtrace();
}

void fun(const struct sigaction* action) {
  sigaction(SIGSEGV, action, nullptr);
  raise(SIGSEGV);
}

int main(int , char ** ) {
  struct sigaction act;
  act.sa_flags = SA_RESETHAND;
  act.sa_sigaction = sig_handler;

  fun(&act);
  return 0;
}