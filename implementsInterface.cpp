#include <type_traits>

template <typename T>
struct Interface {
  using type = T;
};

struct MyOtherType{};

struct MyType : public Interface<MyOtherType> {};

template <typename T>
struct MyNestedType {};

template <typename T, typename = std::void_t<>>
struct HasType : std::false_type {};

template <typename T>
struct HasType<T, std::void_t<typename T::type>> : std::true_type {};


template <typename T, typename = std::void_t<>>
struct FrameWorkNested;

template <template <typename> typename Outer, typename Inner>
struct FrameWorkNested<Outer<Inner>, std::enable_if_t<HasType<Inner>::value>> {
  static_assert(std::is_same_v<typename Inner::type, MyOtherType>);
  using inner_type = Inner;
};

static_assert(std::is_same_v<MyType::type, MyOtherType>);
static_assert(HasType<MyType>::value);

int main() {
  FrameWorkNested<MyNestedType<MyType>>{};
  return 0;
}