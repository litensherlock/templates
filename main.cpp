#include <iostream>
#include <type_traits>
#include <unordered_map>
#include <functional>
#include <new>
#include <memory>

struct MyStatefulCloser {
  void operator()(int & i) {
      ++counter;
      std::cout << "closing " << i << " as number " << counter << std::endl;
  }

  int counter = 0;
};

struct MyCloser {
  void operator()(int & i) {
      std::cout<< "closing " << i << std::endl;
  }
};

template <typename T>
struct DefaultCloser {
  void operator()(T &) {
      std::cout<< "default closing something" << std::endl;
  }
};

template <typename T>
struct DefaultCloser<T*> {
  void operator()(T * ptr) {
      std::cout<< "default deleting " << ptr << std::endl;
      delete ptr;
  }
};

/*
 * the intended for primitive types
 */
template <typename T>
struct unique_value_zero {
  inline static T const zero = T{};
};

template <typename T>
struct unique_value_zero<T*> {
  inline static T * const zero = nullptr;
};

template <typename ValueType>
class unique_value_base {
public:
  explicit unique_value_base(ValueType m_value) :
    m_value{std::move(m_value)} {}

  unique_value_base(unique_value_base &&rhs) noexcept {
      m_value = std::move(rhs.m_value);
      rhs.m_value = unique_value_zero<ValueType>::zero;
  }

  unique_value_base& operator=(unique_value_base && rhs) noexcept {
      this->m_value = std::move(rhs.m_value);
      rhs.m_value = unique_value_zero<ValueType>::zero;
      return *this;
  }

  template <typename U>
  std::enable_if_t<std::is_assignable_v<ValueType, U>,
  ValueType> reset(U&& new_value) {
      ValueType tmp{std::move(m_value)};
      m_value = std::forward<U>(new_value);
      return tmp;
  }
protected:
  ValueType m_value;
};

template <typename ValueType, typename CloserType = DefaultCloser<ValueType>,
    bool = std::is_empty_v<CloserType> && std::is_trivially_default_constructible_v<CloserType>>
class unique_value : public unique_value_base<ValueType> {
  using Base = unique_value_base<ValueType>;

public:
  template <typename T>
  unique_value(ValueType value, T&& closer) :
      Base{value},
      m_closer(std::forward<T>(closer)) {}

  ~unique_value() {
      m_closer(this->m_value);
  }

  unique_value(unique_value && rhs) noexcept: Base(std::move(rhs)) {
      m_closer = rhs.m_closer;
  }

  unique_value& operator=(unique_value && rhs) noexcept {
      Base::operator=(std::move(rhs));
      m_closer = rhs.m_closer;
      return *this;
  }

private:
  CloserType m_closer;
};

template <typename ValueType, typename CloserType>
class unique_value<ValueType, CloserType *, false> : public unique_value_base<ValueType> {
  using Base = unique_value_base<ValueType>;
public:
  unique_value(ValueType value, CloserType * closer) :
    unique_value_base<ValueType>{std::move(value)},
    m_closer(closer) {}

  ~unique_value() {
      m_closer(this->m_value);
  }

  unique_value(unique_value && rhs) noexcept: Base(std::move(rhs.m_value)) {
      m_closer = rhs.m_closer;
  }

  unique_value& operator=(unique_value && rhs) noexcept {
      Base::operator=(std::move(rhs));
      m_closer = rhs.m_closer;
      return *this;
  }

private:
  CloserType * m_closer;
};

template <typename ValueType, typename CloserType>
class unique_value<ValueType, CloserType, true> : public unique_value_base<ValueType> {
  using Base = unique_value_base<ValueType>;
public:
  explicit unique_value(ValueType value) : Base(std::move(value)) {}

  ~unique_value() {
      CloserType{}(this->m_value);
  }

  unique_value(unique_value &&rhs) noexcept: Base(std::move(rhs)) {}

  unique_value& operator=(unique_value && rhs) noexcept  {
      Base::operator=(std::move(rhs));
      return *this;
  }
};

template <typename Value, typename Closer>
unique_value<Value, Closer> make_unique_value(Value v, Closer&& closer) {
    if constexpr (std::is_empty_v<Closer> && std::is_trivially_default_constructible_v<Closer>) {
        return unique_value<Value, Closer>(v);
    } else {
        return unique_value<Value, Closer>(v, std::forward<Closer>(closer));
    }
}

template <typename Value>
unique_value<Value> make_unique_value(Value v) {
    return unique_value<Value>(v);
}

void my_closer_function(int i) {
    std::cout<< "functionType pointer closer closing " << i << std::endl;
}

struct A {
  void a (){
      my_closer_function(2);
  }
};

struct B {
  void operator() (B *) {}
  void myTestFunc(A*) {
    std::cout<< "B::myTestFunc" << std::endl;
  };
};

struct TEST {
  void t(B*) {
    std::cout<< "TEST::myTestFunc" << std::endl;
  }

  void q(A*, B*) {
    std::cout<< "TEST::genericImpl" << std::endl;
  }
};

struct AsyncInterface {
  virtual void function(A* a, B*) = 0;

  virtual ~AsyncInterface() = default;
};

struct Interface2 {
  virtual void not_function(B*) = 0;
};

template <typename F>
struct MFTypes;

template <typename R, typename C, typename... Args>
struct MFTypes<R(C::*)(Args...)> {
  using return_type = R;
  using class_type = C;

  static constexpr std::size_t arity = sizeof...(Args);

  template <std::size_t N>
  struct argument
  {
    static_assert(N < arity, "error: invalid parameter index.");
    using type = typename std::tuple_element<N,std::tuple<Args...>>::type;
  };
};

template <typename FunctionType, size_t argument_index>
using argument_t = typename MFTypes<FunctionType>:: template argument<argument_index>::type;

template <typename FunctionType>
using return_t = typename MFTypes<FunctionType>::return_type;

template <typename FunctionType>
using class_t = typename MFTypes<FunctionType>::class_type ;

template <typename FunctionType, FunctionType functionType, size_t... Is>
class GenericImplementer : public AsyncInterface {
  static_assert(std::is_member_function_pointer_v<FunctionType>);
public:
  ~GenericImplementer() override = default;

  explicit GenericImplementer(class_t<FunctionType> * const obj) : obj{obj} {}

  return_t<FunctionType> function(argument_t<FunctionType, Is>... args) override {
    return (obj->*functionType)(args...);
  };

private:
  class_t<FunctionType> * const obj;
};

template <typename T, auto f, size_t... Is>
AsyncInterface * forwardingFunction(T* obj, std::index_sequence<Is...>) {
  return new GenericImplementer<decltype(f), f, Is...>(obj);
}

template <auto ttf>
AsyncInterface * make_interface(class_t<decltype(ttf)> * const obj) {
  return forwardingFunction<class_t<decltype(ttf)>, ttf>(
      obj, std::make_index_sequence<MFTypes<decltype(ttf)>::arity>{});
}


template <typename T, typename = std::void_t<>>
struct HasP : std::false_type {};

template <typename T>
struct HasP<T, std::void_t<decltype(std::declval<T>()(std::declval<std::add_pointer<A>::type>()))>> : std::true_type {};

#include <tuple>

template <typename... Mixins>
struct T : Mixins... {
  void clear() {
//    (Mixins::clear(), ...);
    int dummy[] = {(void(Mixins::clear()), 0)... , 0};
    (void)dummy;
  }
};

struct AAA {
  void clear() {
    printf("clear A\n");
  }
};


struct BBB {
  void clear() {
    printf("clear B\n");
  }
};

int main() {
  T<AAA, BBB> t;
  t.clear();
  auto * a = new A{};
  auto * b = new B{};
  auto * test = new TEST{};

  AsyncInterface * g = make_interface<&TEST::q>(test);
  g->function(a,b);
  delete g;
  delete a;
  delete b;
  delete test;

  std::cout<< MFTypes<decltype(&TEST::q)>::arity << std::endl;

  int i = 0;
  MyStatefulCloser closer{};
  auto stateful = make_unique_value(i++, MyStatefulCloser{});
  auto stateless = make_unique_value(i++, MyCloser{});
  auto statelessDefault = make_unique_value(i++);
  auto statefulRef = make_unique_value(i++, closer);
  MyStatefulCloser closer2{};
  auto statefulMove = make_unique_value(i++, std::move(closer2));

  auto my_unique_ptr = make_unique_value(new int);
  auto my_second_unique_ptr = std::move(my_unique_ptr);
  auto functionPtr = make_unique_value(i++, &my_closer_function);

  std::cout<< "sizeof(stateful): " << sizeof(stateful) << std::endl;
  std::cout<< "sizeof(stateless): " << sizeof(stateless) << std::endl;
  std::cout<< "sizeof(statelessDefault): " << sizeof(statelessDefault) << std::endl;
  std::cout<< "sizeof(statefulRef): " << sizeof(statefulRef) << std::endl;
  std::cout<< "sizeof(statefulMove): " << sizeof(statefulMove) << std::endl;
  std::cout<< "sizeof(my_unique_ptr): " << sizeof(my_unique_ptr) << std::endl;
  std::cout<< "sizeof(my_second_unique_ptr): " << sizeof(my_second_unique_ptr) << std::endl;
  std::cout<< "sizeof(functionPtr): " << sizeof(functionPtr) << std::endl;

  std::string key{"key"};
  std::unordered_map<std::string, int> string_map;
  string_map.insert(std::make_pair(key, 1));
  {
    auto keyHolder = make_unique_value(key, [&string_map](std::string &s) -> void {
      std::cout<< "running string deleter" << std::endl;
      std::cout<< s << std::endl;
      if (!s.empty()) {
        string_map.erase(s);
      }
    });
    keyHolder.reset(42);
  }

  auto p = new A();
  p->a();

  std::cout << std::boolalpha << "HasP: " << HasP<B>::value << std::endl;
  delete p;
}