#include <vector>
#include <random>
#include <iostream>

#include "MyAllocator.h"
#include "MyVector.h"

int main() {
  long sum = 0;
  std::random_device rd;

  std::vector<int> v2;
  MyVector<int, MyAllocator<int>> v;
  const int stop = 100;
  for (int i = 0; i < stop; ++i) {
    v.push_back(rd());
  }

  for (const int i : v) {
    sum += i;
  }
  v.clear();
  v.shrink_to_fit();
  v2 = {2, 2, 2};
  v.assign(v2.data() ,v2.data() + 2);
  v.insert(v.begin() + 1, 5);
  v.insert(v.begin(), 2, 7);
  std::cout << sum;
  return 0;
}