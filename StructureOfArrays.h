#include <vector>

class SoA;

struct Const: std::true_type {};
struct NonConst: std::false_type{};

class SoA {
  template <typename isConst>
  class MySoaIterator {
    using ParrentType = typename std::conditional<isConst::value, const SoA *,  SoA *>::type;

    template <typename>
    friend class MySoaIterator;

    using id_type = typename std::conditional<isConst::value, const int&, int&>::type;
    using is_cool_type = typename std::conditional<isConst::value, const int&, int&>::type;
    using value_type = typename std::conditional<isConst::value, const double&, double&>::type;
  public:
    using iterator_category = std::random_access_iterator_tag;
    using difference_type = int;

    MySoaIterator(const int it, ParrentType soa) noexcept :
        m_cur(it),
        m_parent(soa)
    {}

    template <typename otherConst, typename = std::enable_if_t<
        std::is_convertible<typename MySoaIterator<otherConst>::ParrentType, ParrentType>::value>>
    MySoaIterator(const MySoaIterator<otherConst> & rhs) noexcept :
        m_cur(rhs.m_cur),
        m_parent(rhs.m_parent)
    {}

    MySoaIterator& operator++() noexcept {
      ++m_cur;
      return *this;
    }

    MySoaIterator& operator--() noexcept {
      --m_cur;
      return *this;
    }

    MySoaIterator operator+(const int steps) noexcept {
      return MySoaIterator(m_cur + steps, m_parent);
    }


    MySoaIterator operator-(const int steps) noexcept {
      return MySoaIterator(m_cur - steps, m_parent);
    }

    bool operator==(const MySoaIterator& rhs) const noexcept {
      return (m_cur == rhs.m_cur) && (m_parent == rhs.m_parent);
    }

    bool operator!=(const MySoaIterator& rhs) const noexcept {
      return !(*this == rhs);
    }

    MySoaIterator* operator->() noexcept {
      return this;
    }

    MySoaIterator& operator*() noexcept {
      return *this;
    }

    difference_type operator-(const MySoaIterator& rhs) const noexcept {
      return m_cur - rhs.m_cur;
    }

    value_type value() {
      return m_parent->m_values[m_cur];
    }

    is_cool_type isCool() {
      return m_parent->m_isCools[m_cur]; // this breaks if m_isCools is a std::vector<bool>
    }

    id_type id() {
      return m_parent->m_ids[m_cur];
    }

  private:
    int m_cur;
    ParrentType m_parent;
  };

public:
  using iterator = MySoaIterator<NonConst>;
  using const_iterator = MySoaIterator<Const>;

  void add(int id, int isCool, double value) {
    m_ids.emplace_back(id);
    m_isCools.emplace_back(isCool);
    m_values.emplace_back(value);
  }

  iterator begin() noexcept {
    return iterator(0, this);
  }

  iterator end() noexcept {
    return iterator(m_ids.size(), this);
  }

  const_iterator begin() const noexcept {
    return const_iterator(0, this);
  }

  const_iterator end() const noexcept {
    return const_iterator(m_ids.size(), this);
  }

  size_t size() const noexcept {
    return m_ids.size();
  }

  void erase(const_iterator pos) {
    const int idx = pos - begin();
    m_ids.erase(m_ids.begin() + idx);
    m_isCools.erase(m_isCools.begin() + idx);
    m_values.erase(m_values.begin() + idx);
  }

private:
  friend iterator;
  friend const_iterator;
private:
  std::vector<int> m_ids;
  std::vector<int> m_isCools;
  std::vector<double> m_values;
};