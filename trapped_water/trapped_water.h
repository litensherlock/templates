
#include <numeric>
#include <iterator>
#include <cassert>
#include <vector>

template <typename BiDirectionalIterator>
typename std::iterator_traits<BiDirectionalIterator>::value_type
calculate_trapped_water(BiDirectionalIterator begin, BiDirectionalIterator end) {
  using value_type = typename std::iterator_traits<BiDirectionalIterator>::value_type;
  using difference_type = typename std::iterator_traits<BiDirectionalIterator>::difference_type;

  const difference_type size = std::distance(begin, end);
  std::vector<value_type> prefixSum;
  std::vector<value_type> postfixSum;
  prefixSum.reserve(size);
  postfixSum.resize(size);

  value_type max = std::numeric_limits<value_type>::min();
  for(auto it = begin; it != end; ++it) {
    max = std::max(max, *it);
    prefixSum.emplace_back(max);
  }

  max = std::numeric_limits<value_type>::min();
  auto insertIt = std::make_reverse_iterator(postfixSum.end());
  for(auto it = std::make_reverse_iterator(end); it != std::make_reverse_iterator(begin); ++it, ++insertIt) {
    max = std::max(max, *it);
    *insertIt = max;
  }

  assert(prefixSum.size() == postfixSum.size());
  assert(static_cast<difference_type>(prefixSum.size()) == size);
  value_type sum = 0;
  for(difference_type i = 0; i < size; ++i, ++begin) {
    const value_type min = std::min(prefixSum[i], postfixSum[i]);
    const value_type height = *begin;
    sum += min - height;
  }

  return sum;
}