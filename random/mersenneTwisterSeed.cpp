
#include <random>
#include <array>
#include <algorithm> // std::generate_n
#include <functional> // std::ref

int main() {
  std::random_device rd;
  std::array<int, std::mt19937::state_size> seed_data;
  std::generate_n(seed_data.data(), seed_data.size(), std::ref(rd));
  std::seed_seq seq(std::begin(seed_data), std::end(seed_data));
  std::mt19937 gen(seq);
  return gen.operator()();
}