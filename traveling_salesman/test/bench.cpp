#include <benchmark/benchmark.h>

#include "../nvidia_cpu.h"
#include "../held_karp.h"

static void BM_nvidia_tsm(benchmark::State & state) {
  const int nodes = state.range(0);
  const int N = nodes * nodes;
  std::vector<int> costs(N, 1000);

  for (int i = 0; i < nodes; ++i) {
    costs.at(i*nodes + ((i+1)%nodes)) = 1;
    costs.at(((i+1)%nodes)*nodes + i) = 1;
  }

  for (auto _ : state) {
    // This code gets timed
    benchmark::DoNotOptimize(nvidia_tsm::find_best_route(costs.data(), nodes));
  }
}

static void BM_held_karp(benchmark::State & state) {
  const int nodes = state.range(0);
  const int N = nodes * nodes;
  std::vector<int> costs(N, 1000);

  for (int i = 0; i < nodes; ++i) {
    costs.at(i*nodes + ((i+1)%nodes)) = 1;
    costs.at(((i+1)%nodes)*nodes + i) = 1;
  }

  for (auto _ : state) {
    // This code gets timed
    benchmark::DoNotOptimize(held_karp::cheapest_route(costs.data(), nodes));
  }
}

// Register the function as a benchmark
BENCHMARK(BM_nvidia_tsm)->Range(3, 11)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_nvidia_tsm)->Arg(13)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_held_karp)->Range(3, 11)->Unit(benchmark::kMillisecond);;
BENCHMARK(BM_held_karp)->Arg(13)->Unit(benchmark::kMillisecond);;

// Run the benchmark
BENCHMARK_MAIN();