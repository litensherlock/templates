#include <gtest/gtest.h>

#include "../held_karp.h"
#include <array>

TEST(traveling_salesman_held_karp, small_test) {
  const int nodes = 3;
  const int N = nodes * nodes;
  std::array<int, N> costs{};
  costs.fill(1000);

  for (int i = 0; i < nodes; ++i) {
    costs.at(i*nodes + ((i+1)%nodes)) = 1;
    costs.at(((i+1)%nodes)*nodes + i) = 1;
  }

  held_karp::route_cost route = held_karp::cheapest_route(costs.data(), nodes);

  EXPECT_EQ(nodes, route.cost);
}

TEST(traveling_salesman_held_karp, bigger_test) {
  const int nodes = 13;
  const int N = nodes * nodes;
  std::array<int, N> costs{};
  costs.fill(1000);

  for (int i = 0; i < nodes; ++i) {
    costs.at(i*nodes + ((i+1)%nodes)) = 1;
    costs.at(((i+1)%nodes)*nodes + i) = 1;
  }

  held_karp::route_cost route = held_karp::cheapest_route(costs.data(), nodes);

  EXPECT_EQ(nodes, route.cost);
}

TEST(traveling_salesman_held_karp, test_10) {
  const int nodes = 10;
  const int N = nodes * nodes;
  std::array<int, N> costs{};
  costs.fill(1000);

  for (int i = 0; i < nodes; ++i) {
    costs.at(i*nodes + ((i+1)%nodes)) = 1;
    costs.at(((i+1)%nodes)*nodes + i) = 1;
  }

  held_karp::route_cost route = held_karp::cheapest_route(costs.data(), nodes);

  EXPECT_EQ(nodes, route.cost);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}