#include <gtest/gtest.h>

#include "../nvidia_cpu.h"
#include <array>
#include <iostream>

TEST(nvidia_tsm, small_test) {
  const int nodes = 3;
  const int N = nodes * nodes;
  std::array<int, N> costs{};
  costs.fill(1000);

  for (int i = 0; i < nodes; ++i) {
    costs.at(i*nodes + ((i+1)%nodes)) = 1;
    costs.at(((i+1)%nodes)*nodes + i) = 1;
  }

  nvidia_tsm::route_cost route = nvidia_tsm::find_best_route(costs.data(), nodes);

  nvidia_tsm::route_iterator it(route.route, nodes);
  while (!it.done()) {
    std::cout << it.next();
    if (!it.done()) {
      std::cout << " -> ";
    }
  }
  std::cout << "\n\n";

  EXPECT_EQ(nodes - 1, route.cost);
}


TEST(nvidia_tsm, bigger_test) {
  const int nodes = 11;
  const int N = nodes * nodes;
  std::array<int, N> costs{};
  costs.fill(1000);

  for (int i = 0; i < nodes; ++i) {
    costs.at(i*nodes + ((i+1)%nodes)) = 1;
    costs.at(((i+1)%nodes)*nodes + i) = 1;
  }

  nvidia_tsm::route_cost route = nvidia_tsm::find_best_route(costs.data(), nodes);
  EXPECT_EQ(nodes - 1, route.cost);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}