#ifndef TEMPLATES__NVIDIA_CPU_H
#define TEMPLATES__NVIDIA_CPU_H

#include <limits>

namespace nvidia_tsm {

namespace detail {
constexpr unsigned long factorial(unsigned int n) {
  unsigned long res = 1;
  for (; n > 1; --n) {
    res *= n;
  }
  return res;
}
} // detail

struct route_cost {
  long route;
  int cost;

  route_cost() : route(-1), cost(std::numeric_limits<int>::max()) {}

  route_cost(long route, int cost) : route(route), cost(cost) {}

  static struct min_class {
    route_cost operator()(const route_cost &a, const route_cost &b) {
      return a.cost < b.cost ? a : b;
    }
  } min;

  static route_cost minf(const route_cost &a, const route_cost &b) {
    return a.cost < b.cost ? a : b;
  }
};

struct route_iterator {
  long reminder;
  int hops_left;
  unsigned visited = 0;

  route_iterator(long route_id, int num_hops)
      : reminder(route_id),
        hops_left(num_hops) {}

  bool done() const {
    return hops_left <= 0;
  }

  int first() {
    int index = (int) (reminder % hops_left);
    reminder /= hops_left;
    --hops_left;
    visited = (1 << index);
    return index;
  }

  int next() {
    long available = reminder % hops_left;
    reminder /= hops_left;
    --hops_left;
    int index = 0;
    while (true) {
      if ((visited & (1 << index)) == 0) {
        if (--available < 0) {
          break;
        }
      }
      ++index;
    }
    visited |= (1 << index);
    return index;
  }
};

/**
 * taken from this video https://www.youtube.com/watch?v=cbbKEAWf1ow&list=PLVFrD1dmDdvcOwDsNigchYDgBKIWFpcXK&index=3&t=18m28s
 */
route_cost find_best_route(const int *distances, int N) {
  long num_routes = detail::factorial(N);
  route_cost best_route;

  for (long i = 0; i < num_routes; ++i) {
    int cost = 0;
    route_iterator it(i, N);
    int from = it.first();
    while (!it.done()) {
      int to = it.next();
      cost += distances[from * N + to];
      from = to;
    }
    best_route = route_cost::minf(best_route, route_cost(i, cost));
  }

  return best_route;
}

} // nvidia_tsm

#endif //TEMPLATES__NVIDIA_CPU_H
