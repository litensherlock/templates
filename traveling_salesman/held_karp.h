#ifndef TEMPLATES__HELD_KARP_H
#define TEMPLATES__HELD_KARP_H

#include <vector>
#include <limits>

namespace held_karp {

struct route_cost {
  const int cost;

  route_cost(int cost) : cost(cost) {}
};

namespace detail {
void held_karp(const int * distance, const int N, std::vector<std::vector<int>> & best) {
  // use DP to solve all states
  // note that whenever we solve a particular state,
  // the smaller states we need have already been solved

  for (int visited = 1; visited < (1<<(N-1)); ++visited) {
    for (int last = 0; last < (N-1); ++last) {

      // last visited vertex must be one of the visited vertices
      if (!(visited & 1<<last))
        continue;

      // try all possibilities for the previous vertex,
      // pick the best among them
      if (visited == 1 << last) {
        // previous vertex must have been N-1
        best[visited][last] = distance[(N-1)*N + last];
      } else {
        // previous vertex was one of the other ones in "visited"
        int prev_visited = visited ^ 1<<last;
        for (int prev = 0; prev < N-1; ++prev) {
          if (!(prev_visited & 1<<prev))
            continue;

          best[visited][last] = std::min(best[visited][last], distance[last*N + prev] + best[prev_visited][prev]);
        }
      }
    }
  }
}
}


/**
 * taken from https://www.quora.com/Are-there-any-good-examples-of-the-Held-Karp-algorithm-in-C++-Hard-to-find-example-code-to-solve-the-traveling-salesman-problem-Everyone-wants-to-just-talk-about-theory-and-not-show-how-to-actually-do-it-What-is-the-big-secret/answer/Michal-Fori%C5%A1ek
 */
route_cost cheapest_route(const int * distance, const int N) {
  // initialize the DP table
  // best[visited][last] = the cheapest way to reach the state where:
  // - "visited" encodes the set of visited vertices other than N-1
  // - "last" is the number of the vertex visited last

  std::vector< std::vector<int> > best( 1<<(N-1), std::vector<int>( N, std::numeric_limits<int>::max() ) );
  detail::held_karp(distance, N, best);

  // use the precomputed path lengths to choose the cheapest cycle
  int answer = std::numeric_limits<int>::max();
  for (int last=0; last<N-1; ++last) {
    answer = std::min(
        answer,
        distance[last*N  + (N-1)] + best[ (1<<(N-1))-1 ][last]
    );
  }

  return route_cost(answer);
}

} // held_karp

#endif //TEMPLATES__HELD_KARP_H
