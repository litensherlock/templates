#ifndef TEMPLATES__MYALLOCATOR_H
#define TEMPLATES__MYALLOCATOR_H

#include <new>
#include <iostream>

//void* operator new(std::size_t t, std::align_val_t) {
//  std::cout << "my malloc new was called \n";
//  return malloc(t);
//}
//
//void* operator new(const std::size_t size, int * const __p) {
//  std::cout << "my placement new was called \n";
//  return ::operator new(size, static_cast<void*>(__p));
//}

template <typename T>
class MyAllocator {
public:
  typedef size_t     size_type;
  typedef ptrdiff_t  difference_type;
  typedef T*       pointer;
  typedef const T* const_pointer;
  typedef T&       reference;
  typedef const T& const_reference;
  typedef T        value_type;

  [[nodiscard]] T* allocate(const size_t n) {
    if(n == 0) {
      return nullptr;
    }
    void * const ptr = ::operator new(n * sizeof(T), std::align_val_t(alignof(T)));
    std::cout << "allocate n=" << n << " ptr=" << ptr << '\n';
    return static_cast<T*>(ptr);
  }

  void deallocate(T* const ptr, const size_t) noexcept(noexcept(::operator delete(ptr, std::align_val_t(alignof(T))))) {
    if(ptr == nullptr) {
      return;
    }
    std::cout << "deallocate "<< ptr << '\n';
    ::operator delete(ptr, std::align_val_t(alignof(T)));
  }

  // overload for construction without any arguments, default construct without value initialization
  void construct(T* const ptr) noexcept(std::is_nothrow_default_constructible<T>::value) {
    new (ptr) T;
  }
};

#endif //TEMPLATES__MYALLOCATOR_H
